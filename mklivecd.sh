#!/bin/bash

#sudo apt-get install -y \
#    debootstrap squashfs-tools xorriso grub-pc-bin grub-efi-amd64-bin mtools

#mkdir $HOME/LIVE_BOOT

#sudo debootstrap \
#    --arch=i386 \
#    --variant=minbase \
#    stretch \
#    /home/phobos/LIVE_BOOT/chroot \
#    http://ftp.de.debian.org/debian/

#sudo chroot /home/phobos/LIVE_BOOT/chroot/

#mkdir -p /home/phobos/LIVE_BOOT/{scratch,image/live}

sudo mksquashfs \
    /home/phobos/LIVE_BOOT/chroot \
    /home/phobos/LIVE_BOOT/image/live/filesystem.squashfs \
    -e boot

cp /home/phobos/LIVE_BOOT/chroot/boot/vmlinuz* /home/phobos/LIVE_BOOT/image/vmlinuz
cp /home/phobos/LIVE_BOOT/chroot/boot/initrd.img* /home/phobos/LIVE_BOOT/image/initrd

#cat <<'EOF' >$HOME/LIVE_BOOT/scratch/grub.cfg \
#search --set=root --file /DEBIAN_CUSTOM \
#insmod all_video \
#set default="0" \
#set timeout=30 \
#menuentry "Debian Live (Mumbletreff.de)" { \
#    linux /vmlinuz boot=live quiet nomodeset \
#    initrd /initrd \
#} \
#EOF

#touch /home/phobos/LIVE_BOOT/image/DEBIAN_CUSTOM

grub-mkstandalone \
    --format=x86_64-efi \
    --output=$HOME/LIVE_BOOT/scratch/bootx64.efi \
    --locales="" \
    --fonts="" \
    "boot/grub/grub.cfg=$HOME/LIVE_BOOT/scratch/grub.cfg"
    
( \
cd $HOME/LIVE_BOOT/scratch && \
dd if=/dev/zero of=efiboot.img bs=1M count=10 && \
/sbin/mkfs.vfat efiboot.img && \
mmd -i efiboot.img efi efi/boot && \
mcopy -i efiboot.img ./bootx64.efi ::efi/boot/; \
)

grub-mkstandalone \
    --format=i386-pc \
    --output=$HOME/LIVE_BOOT/scratch/core.img \
    --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
    --modules="linux normal iso9660 biosdisk search" \
    --locales="" \
    --fonts="" \
    "boot/grub/grub.cfg=$HOME/LIVE_BOOT/scratch/grub.cfg"

cat /usr/lib/grub/i386-pc/cdboot.img $HOME/LIVE_BOOT/scratch/core.img > $HOME/LIVE_BOOT/scratch/bios.img

xorriso \
    -as mkisofs \
    -iso-level 3 \
    -full-iso9660-filenames \
    -volid "DEBIAN_CUSTOM" \
    -eltorito-boot \
        boot/grub/bios.img \
        -no-emul-boot \
        -boot-load-size 4 \
        -boot-info-table \
        --eltorito-catalog boot/grub/boot.cat \
    --grub2-boot-info \
    --grub2-mbr /usr/lib/grub/i386-pc/boot_hybrid.img \
    -eltorito-alt-boot \
        -e EFI/efiboot.img \
        -no-emul-boot \
    -append_partition 2 0xef ${HOME}/LIVE_BOOT/scratch/efiboot.img \
    -output "${HOME}/LIVE_BOOT/debian-custom.iso" \
    -graft-points \
        "${HOME}/LIVE_BOOT/image" \
        /boot/grub/bios.img=$HOME/LIVE_BOOT/scratch/bios.img \
        /EFI/efiboot.img=$HOME/LIVE_BOOT/scratch/efiboot.img

#ls -lh $HOME/LIVE_BOOT/

